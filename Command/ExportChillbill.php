<?php
declare(strict_types=1);

namespace Cwd\ChillbillBundle\Command;
/*
 * This file is part of ChillBill Bundle
 *
 * (c) 2018 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
use Cwd\ChillbillBundle\Service\ChillbillAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportChillbillCommand extends Command
{
    /** @var ChillbillAdapter */
    private $adapter;

    public function __construct(ChillbillAdapter $adapter)
    {
        $this->adapter = $adapter
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chillbill:export')
            ->setDescription('Export invoices as CSV for BMD import')
            ->addArgument(
                'start',
                InputArgument::OPTIONAL,
                'Start date, like "YYYY-MM-DD" or "last month", "this month"',
                'last month'
            )
            ->addArgument(
                'end',
                InputArgument::OPTIONAL,
                'End date'
            )
            ->addOption(
                'konten',
                'k',
                InputOption::VALUE_OPTIONAL,
                'Optional CSV File with Customer Account Numbers from BMD',
                null
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->exporter->setOutput($output);

        $start = $input->getArgument('start');
        $end = $input->getArgument('end');

        switch ($start) {
            case 'last month':
                $startDate = 'first day of last month 00:00:00';
                $endDate = 'last day of last month 23:59:59';
                break;
            case 'this month':
                $startDate = 'first day of this month 00:00:00';
                $endDate = 'last day of this month 23:59:59';
                break;
            default:
                $startDate = $start;
                $endDate = $end;
        }

        $this->exporter->export($startDate, $endDate, $input->getOption('konten'));
    }
}
