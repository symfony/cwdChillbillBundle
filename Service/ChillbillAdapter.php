<?php


namespace Cwd\ChillbillBundle\Service;


use Cwd\ChillbillBundle\Domain\Invoice;
use GuzzleHttp\Client;

class ChillbillAdapter
{
    private $apiKey;
    private $apiUrl;
    private $client;

    private const API_BILL = 'bills';

    public function __construct(array $config)
    {
        if(!isset($config['api_key']) || empty($config['api_key'])) {
            throw new \Exception('api_key not set or empty');
        }

        if(!isset($config['api_url']) || empty($config['api_url'])) {
            throw new \Exception('api_url not set or empty');
        }        

        $this->apiUrl = $config['api_url'];
        $this->apiKey = $config['api_key'];

        $this->client = new Client();
    }

    public function getBills($afterBillNumber = null)
    {
        $jsonData = $this->call(self::API_BILL, 'GET', ['afterBillNumber' => $afterBillNumber]);
        return $this->mapInvoices($jsonData);
    }

    protected function call($endpoint, $method = 'GET', array $params = [])
    {
        $result = $this->client->request($method, $this->apiUrl.$endpoint, ['auth' => [$this->apiKey, ''], 'query' => $params]);

        if ($result->getStatusCode() !== 200) {
            throw new \Exception(sprintf('Status %s with reason: %s', $result->getStatusCode(), $result->getReasonPhrase()));
        }

        return $content = json_decode($result->getBody()->getContents());

    }

    protected function mapInvoices(array $json)
    {
        $invoices = [];

        foreach ($json as $invoiceData) {
            $invoice = new Invoice();
            $invoice->setId($invoiceData->id)
                    ->setBillNumber($invoiceData->billNumber)
                    ->setInvoiceDate(new \DateTime($invoiceData->invoiceDate))
                    ->setDueDate(new \DateTime($invoiceData->dueDate))
                    ->addOrganization($invoiceData->organization->name, $invoiceData->organization->vatNumber)
                    ->setInvoiceNumber($invoiceData->invoiceNumber)
                    ->setCurrencyCode($invoiceData->currencyCode)
                    ->setDocumentUrl($invoiceData->documentUrl);

            foreach ($invoiceData->amounts as $amountData) {
                $invoice->addAmount($amountData->total, $amountData->vatRate, $amountData->euroTotal);
            }

            $invoices[] = $invoice;
        }

        return $invoices;
    }
}