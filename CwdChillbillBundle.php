<?php
/*
 * This file is part of CwdFancyGridBundle
 *
 * (c)2017 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\ChillbillBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @package Cwd\ChillbillBundle
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class CwdChillbillBundle extends Bundle
{
}
