cwdChillbillBundle
=================

[![Latest Stable Version](https://poser.pugx.org/cwd/chillbill-bundle/v/stable)](https://packagist.org/packages/cwd/chillbill-bundle)
[![Total Downloads](https://poser.pugx.org/cwd/chillbill-bundle/downloads)](https://packagist.org/packages/cwd/chillbill-bundle)
[![License](https://poser.pugx.org/cwd/fancygrid-bundle/license)](https://packagist.org/packages/cwd/chillbill-bundle)


This bundles lets you intecreate the chillbill.co API



Installation
------------

`composer require cwd/chillbill-bundle`

Add to AppKernel.php:
```
[...]
new Cwd\ChillbillBundle\CwdChillbillBundle(),
[...]
```

Or bundels.php:
```
[...]
Cwd\ChillbillBundle\CwdChillbillBundle::class => ['all' => true],
[...]
```

Add to config.yml:
```
cwd_chillbill:
  api_key: '123456'      
```

How to use:
```
$offset = 123; // Optional Offset
$bills = $this->get(ChillbillAdapter::class)->getBills($offset);

foreach ($bills as $bill) {
 // do something with it....
}
```