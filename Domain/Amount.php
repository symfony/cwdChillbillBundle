<?php


namespace Cwd\ChillbillBundle\Domain;


class Amount
{
    /** @var float|null */
    private $total;

    /** @var float|null */
    private $vatRate;

    /** @var float|null */
    private $euroTotal;

    public function __construct($total, $vatRate = null, $euroTotal = null)
    {
        $this->total = $total;
        $this->vatRate = $vatRate;
        $this->euroTotal = $euroTotal;
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float|null $total
     * @return Amount
     */
    public function setTotal(?float $total): Amount
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getVatRate(): ?float
    {
        return $this->vatRate;
    }

    /**
     * @param float|null $vatRate
     * @return Amount
     */
    public function setVatRate(?float $vatRate): Amount
    {
        $this->vatRate = $vatRate;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getEuroTotal(): ?float
    {
        return $this->euroTotal;
    }

    /**
     * @param float|null $euroTotal
     * @return Amount
     */
    public function setEuroTotal(?float $euroTotal): Amount
    {
        $this->euroTotal = $euroTotal;
        return $this;
    }
}