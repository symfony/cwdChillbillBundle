<?php


namespace Cwd\ChillbillBundle\Domain;


class Invoice
{
    /** @var string */
    private $id;

    /** @var int */
    private $billNumber;

    /** @var \DateTime */
    private $invoiceDate;

    /** @var \DateTime|null */
    private $dueDate;

    /** @var Organization */
    private $organization;

    /** @var string */
    private $invoiceNumber;

    /** @var Amounts[] */
    private $amounts = [];

    /** @var string */
    private $currencyCode;

    /** @var string */
    private $documentUrl;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Invoice
     */
    public function setId(string $id): Invoice
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getBillNumber(): int
    {
        return $this->billNumber;
    }

    /**
     * @param int $billNumber
     * @return Invoice
     */
    public function setBillNumber(int $billNumber): Invoice
    {
        $this->billNumber = $billNumber;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate(): \DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @param \DateTime $invoiceDate
     * @return Invoice
     */
    public function setInvoiceDate(\DateTime $invoiceDate): Invoice
    {
        $this->invoiceDate = $invoiceDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime|null $dueDate
     * @return Invoice
     */
    public function setDueDate(?\DateTime $dueDate): Invoice
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization(): Organization
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     * @return Invoice
     */
    public function setOrganization(Organization $organization): Invoice
    {
        $this->organization = $organization;
        return $this;
    }

    public function addOrganization($name, $vatNumber = null): Invoice
    {
        $this->organization = new Organization($name, $vatNumber);
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber(): string
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return Invoice
     */
    public function setInvoiceNumber(string $invoiceNumber): Invoice
    {
        $this->invoiceNumber = $invoiceNumber;
        return $this;
    }

    /**
     * @return Amounts[]
     */
    public function getAmounts(): array
    {
        return $this->amounts;
    }

    /**
     * @param Amounts[] $amounts
     * @return Invoice
     */
    public function setAmounts(array $amounts): Invoice
    {
        $this->amounts = $amounts;
        return $this;
    }

    public function addAmount($total, $vatRate, $euroTotal): Invoice
    {
        $this->amounts[] = new Amount($total, $vatRate, $euroTotal);
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentUrl(): string
    {
        return $this->documentUrl;
    }

    /**
     * @param string $documentUrl
     * @return Invoice
     */
    public function setDocumentUrl(string $documentUrl): Invoice
    {
        $this->documentUrl = $documentUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return Invoice
     */
    public function setCurrencyCode(string $currencyCode): Invoice
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }
}