<?php


namespace Cwd\ChillbillBundle\Domain;


class Organization
{
    /** @var string */
    private $name;

    /** @var string|null */
    private $vatNumber;

    public function __construct($name, $vatNumber = null)
    {
        $this->name = $name;
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Organization
     */
    public function setName(string $name): Organization
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    /**
     * @param null|string $vatNumber
     * @return Organization
     */
    public function setVatNumber(?string $vatNumber): Organization
    {
        $this->vatNumber = $vatNumber;
        return $this;
    }


}